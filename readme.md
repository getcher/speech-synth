# Yandex TTS API Wrapper

This is a simple Flask application that wraps Yandex TTS API and exposes a RESTful API endpoint for synthesizing text-to-speech.

## Prerequisites

*   A Yandex account with TTS API access.
*   The API key for TTS API access.
*   Flask, Requests, and Argparse packages installed.

## Installation

1.  Clone the repository to your local machine:

<pre>

<div class="bg-black mb-4 rounded-md">

<div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans"><span class="">shell</span><button class="flex ml-auto gap-2">Copy code</button></div>

<div class="p-4 overflow-y-auto">`<span class="hljs-meta prompt_">$</span> <span class="bash">git <span class="hljs-built_in">clone</span> https://github.com/<your-username>/yandex-tts-api-wrapper.git</span>` </div>

</div>

</pre>

1.  Change into the directory:

<pre>

<div class="bg-black mb-4 rounded-md">

<div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans"><span class="">shell</span><button class="flex ml-auto gap-2">Copy code</button></div>

<div class="p-4 overflow-y-auto">`<span class="hljs-meta prompt_">$</span> <span class="bash"><span class="hljs-built_in">cd</span> yandex-tts-api-wrapper</span>` </div>

</div>

</pre>

1.  Install the required packages:

<pre>

<div class="bg-black mb-4 rounded-md">

<div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans"><span class="">ruby</span><button class="flex ml-auto gap-2">Copy code</button></div>

<div class="p-4 overflow-y-auto">`<span class="hljs-variable">$</span> pip install -r requirements.txt` </div>

</div>

</pre>

## Usage

1.  Set the environment variables:

<pre>

<div class="bg-black mb-4 rounded-md">

<div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans"><span class="">shell</span><button class="flex ml-auto gap-2">Copy code</button></div>

<div class="p-4 overflow-y-auto">`<span class="hljs-meta prompt_">$</span> <span class="bash"><span class="hljs-built_in">export</span> folder_id=<your-yandex-folder-id></span>
<span class="hljs-meta prompt_">$</span> <span class="bash"><span class="hljs-built_in">export</span> IAM_TOKEN=<your-yandex-iam-token></span>` </div>

</div>

</pre>

1.  Start the Flask application:

<pre>

<div class="bg-black mb-4 rounded-md">

<div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans"><span class="">ruby</span><button class="flex ml-auto gap-2">Copy code</button></div>

<div class="p-4 overflow-y-auto">`<span class="hljs-variable">$</span> python app.py` </div>

</div>

</pre>

1.  Test the endpoint by sending a POST request:

<pre>

<div class="bg-black mb-4 rounded-md">

<div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans"><span class="">json</span><button class="flex ml-auto gap-2">Copy code</button></div>

<div class="p-4 overflow-y-auto">`$ curl -X POST -H <span class="hljs-string">"Content-Type: application/json"</span> -d '<span class="hljs-punctuation">{</span><span class="hljs-attr">"text"</span><span class="hljs-punctuation">:</span> <span class="hljs-string">"Hello, World!"</span><span class="hljs-punctuation">,</span> <span class="hljs-attr">"voice"</span><span class="hljs-punctuation">:</span> <span class="hljs-string">"filipp"</span><span class="hljs-punctuation">}</span>' http<span class="hljs-punctuation">:</span><span class="hljs-comment">//localhost:5000/synthesize</span>` </div>

</div>

</pre>

## Deployment

This application can be easily deployed to a cloud platform such as Heroku. Follow the steps below to deploy:

1.  Create a new Heroku app:

<pre>

<div class="bg-black mb-4 rounded-md">

<div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans"><span class="">lua</span><button class="flex ml-auto gap-2">Copy code</button></div>

<div class="p-4 overflow-y-auto">`$ heroku <span class="hljs-built_in">create</span> <your-app-name>` </div>

</div>

</pre>

1.  Set the environment variables in Heroku:

<pre>

<div class="bg-black mb-4 rounded-md">

<div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans"><span class="">shell</span><button class="flex ml-auto gap-2">Copy code</button></div>

<div class="p-4 overflow-y-auto">`<span class="hljs-meta prompt_">$</span> <span class="bash">heroku config:<span class="hljs-built_in">set</span> folder_id=<your-yandex-folder-id></span>
<span class="hljs-meta prompt_">$</span> <span class="bash">heroku config:<span class="hljs-built_in">set</span> IAM_TOKEN=<your-yandex-iam-token></span>` </div>

</div>

</pre>

1.  Deploy the application:

<pre>

<div class="bg-black mb-4 rounded-md">

<div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans"><span class="">perl</span><button class="flex ml-auto gap-2">Copy code</button></div>

<div class="p-4 overflow-y-auto">`$ git <span class="hljs-keyword">push</span> heroku master` </div>

</div>

</pre>

1.  Test the endpoint:

<pre>

<div class="bg-black mb-4 rounded-md">

<div class="flex items-center relative text-gray-200 bg-gray-800 px-4 py-2 text-xs font-sans"><span class="">json</span><button class="flex ml-auto gap-2">Copy code</button></div>

<div class="p-4 overflow-y-auto">`$ curl -X POST -H <span class="hljs-string">"Content-Type: application/json"</span> -d '<span class="hljs-punctuation">{</span><span class="hljs-attr">"text"</span><span class="hljs-punctuation">:</span> <span class="hljs-string">"Hello, World!"</span><span class="hljs-punctuation">,</span> <span class="hljs-attr">"voice"</span><span class="hljs-punctuation">:</span> <span class="hljs-string">"filipp"</span><span class="hljs-punctuation">}</span>' https<span class="hljs-punctuation">:</span><span class="hljs-comment">//<your-app-name>.herokuapp.com/synthesize</span>` </div>

</div>

</pre>